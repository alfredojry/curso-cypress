describe('Tickets', () => {
    beforeEach(() => cy.visit('https://ticket-box.s3.eu-central-1.amazonaws.com/index.html'));
    
    it('Fill all the text input fields', () => {
        const firstName = 'Alfredo';
        const lastName = 'Jimenez';
        const email = 'ing.yjimenez@gmail.com';

        cy.get('#first-name').type(firstName);
        cy.get('#last-name').type(lastName);
        cy.get('#email').type(email);
        cy.get('#requests').type('Lorem Ipsum');
        cy.get('#signature').type(`${firstName} ${lastName}`);
    });

    it('Select two tickets', () => {
        cy.get('#ticket-quantity').select('2');
    });

    it('Select \'VIP\' ticket type', () => {
        cy.get('#vip').check();
    });

    it('select \'Social Media\' checkboxes', () => {
        cy.get('#social-media').check();
    });

    it('select \'friend\' and \'publication\', then uncheck \'friend\'', () => {
        cy.get('#friend').check();
        cy.get('#publication').check();
        cy.get('#friend').uncheck();
    });

    it('Has \'TICKETBOX\' header\'s heading', () => {
        cy.get('header h1').should('contain', 'TICKETBOX');
    });

    it('Alert on invalid email', () => {
        cy.get('#email')
        .as('email')
        .type('alfredo$gmail.com');
        // cy.get('#email').should('class', 'invalid');
        
        cy.get('#email.invalid').should('exist');

        cy.get('@email')
            .clear()
            .type('ing.yjimenez@gmail.com');
        
        cy.get('#email.invalid').should('not.exist');
    });

    it('Fill and reset the form', () => {
        const firstName = 'Alfredo';
        const lastName = 'Jimenez';
        const email = 'ing.yjimenez@gmail.com';
        const fullName = `${firstName} ${lastName}`;

        cy.get('#first-name').type(firstName);
        cy.get('#last-name').type(lastName);
        cy.get('#email').type(email);
        cy.get('#ticket-quantity').select('2');
        cy.get('#vip').check();
        cy.get('#friend').check();
        cy.get('#requests').type('Lorem Ipsum');

        cy.get('.agreement p').should(
            'contain',
            `I, ${fullName}, wish to buy 2 VIP tickets.`,
        );

        cy.get('#agree').click();
        cy.get('#signature').type(fullName);

        cy.get('button[type="submit"]')
            .as('submitButton')
            .should('not.be.disabled');
        
        cy.get('button[type="reset"]').click();

        cy.get('@submitButton').should('be.disabled');
    });

    it('Fill mandatory fields using support command', () => {
        const customer = {
            firstName: 'Alfredo',
            lastName: 'Jiménez',
            email: 'ing.yjimenez@gmail.com',
        };

        cy.fillMandatoryFields(customer);

        cy.get('button[type="submit"]')
            .as('submitButton')
            .should('not.be.disabled');
        
        cy.get('#agree').uncheck();

        cy.get('@submitButton').should('be.disabled');
    });
});